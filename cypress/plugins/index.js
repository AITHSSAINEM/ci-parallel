module.exports = (on, config) => {
  // Use a unique parallelization identifier for each CI node or machine
  const parallelizationId = process.env.CI || process.env.CIRCLECI || process.env.CIRCLE_NODE_INDEX || process.env.CI_NODE_TOTAL || process.env.BUILD_NUMBER || process.env.PARALLELIZATION_ID || '';

  // Calculate the number of parallel groups based on the total number of CI nodes or machines
  const parallelGroups = parseInt(process.env.CI_NODE_TOTAL || process.env.PARALLEL_GROUPS || 1, 10);

  // Calculate the current parallel group based on the CI node or machine index
  const currentGroup = parseInt(process.env.CI_NODE_INDEX || process.env.PARALLEL_GROUP || 0, 10);

  // Calculate the number of specs per parallel group
  const specsPerGroup = Math.ceil(config.testFiles.length / parallelGroups);

  // Calculate the starting index for the specs in the current parallel group
  const start = currentGroup * specsPerGroup;

  // Calculate the ending index for the specs in the current parallel group
  const end = Math.min(start + specsPerGroup, config.testFiles.length);

  // Slice the testFiles array to only include the specs for the current parallel group
  config.testFiles = config.testFiles.slice(start, end);

  // Set the parallelization environment variables for use in your tests
  process.env.PARALLELIZATION_ID = parallelizationId;
  process.env.PARALLEL_GROUP = currentGroup.toString();
  process.env.PARALLEL_GROUPS = parallelGroups.toString();

  return config;
};
