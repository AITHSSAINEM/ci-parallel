const { defineConfig } = require("cypress");
const getspecFiles = require("cypress-gitlab-parallel-runner")
const { cloudPlugin } = require("cypress-cloud/plugin");
const allureWriter = require('@shelex/cypress-allure-plugin/writer');

module.exports = defineConfig({
  projectId : 'tohi49',
  key : '33253651-6ac0-488b-998b-24c61164a386',
  vedio : false,
  e2e: {
    setupNodeEvents(on, config) {
      //getspecFiles("../cypress/e2e",true)
      allureWriter(on, config);
      cloudPlugin(on, config);
      return  config;
      

    },
  },
});
